// This is where it all goes :)


//Side navbar toggle method.
$(document).ready(function () {
    // Left sidebar navigation

    function sidebarNavigationGeneration() {
        $('#grouping-dropdown').on('click', function () {
            $("#grouping-dropdown").addClass("caret");
         });
    
    
        $('#release-dropdown').on('click', function () {
            $("#release-dropdown").addClass("caret");
        });
        
        var grouping = '#grouping-submenu li';
        var resources = '#release-notes-submenu li';
        var selector = '#active-sidebar li';
        var url = window.location.pathname;
    
        $(selector).each(function(){
            if($(this).find('a').attr('href')===(url)){
                $(this).find('a').addClass('active');
            }
        })
    
        $(grouping).each(function(){
            if($(this).find('a').attr('href')===(url)){
                $("#grouping-dropdown").addClass("caret");
                $('#grouping-submenu').addClass('show');
                $(this).find('a').addClass('active');
            }
        });
    
        $(resources).each(function() {
            if($(this).find('a').attr('href')===(url)){
                $("#release-dropdown").addClass("caret");
                $('#release-notes-submenu').addClass('show');
                $(this).find('a').addClass('active');
            }
        });

        $(".sidebar-list .nav-link").on("click", function(){
            $(".sidebar-list").find(".active").toggleClass("active");
            $(this).addClass("active");
        });  
    }
    ///////////////////

    //Sidebar Toggling for tab
     function sidebarToggle() {
        $('#sidebarCollapse').on('click', function () {
            $('.sidebar-list').toggleClass('active');
            $('.overlay').toggleClass('active');
            $('#content').toggleClass('active');
        })

        $('.overlay').on('click', function () {
            $('.sidebar-list').removeClass('active');
            $('.overlay').removeClass('active');
            $('#content').removeClass('active');
        });
     }
        

 ////////////////////////////////////////   
    // Scrollspy Navigation

    function scrollSpyNavigation() {
        $('body').scrollspy({ target: '#spy', offset:163});

        $("[ax-scroll]").click(function(){
            $("html, body").animate({scrollTop: $($(this).attr('href')).offset().top - 60}, 500);
        });    
    }

    ////////////////////


    // Scroll spy generation

    function generateScrollSpy(){ 
        var htmlDOM = '<h5 class="scrollspy-heading">' + $("h1").html() + '</h5>' +
                            '<ul class="nav flex-column">'
        
        $("h2,h3").addClass("content-heading").each(function()  {
           htmlDOM +=   '<i class="fa fa-caret-right icon" aria-hidden="true"></i>' +
                        '<li class="nav-item">' + 
                        '<a class="nav-link " ax-scroll href=#' + $(this).attr('id') + '>' +
                            $(this).html() +
                        '</a>' +
                       '</li>';
        });
        htmlDOM += '</ul>';
        $("#spy").html(htmlDOM);

    }

    ////////////////////////


    sidebarNavigationGeneration();

    sidebarToggle();

    generateScrollSpy();

    scrollSpyNavigation();
})


