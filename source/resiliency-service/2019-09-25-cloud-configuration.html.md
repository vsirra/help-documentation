---
title: Cloud Configurations
layout: service_layout
sidebar_heading: Resiliency Service
sidebar_grouping: Concepts
list: how-to-guides
---
### Creating a Cloud Configuration

This section explains how to  **Configure a Cloud** using Appranix Cloud Resiliency Service.

  -  Login to your Appranix account using the credentials you created 
  -  Go to the cloud configuration page 
  -  Select your cloud provider, in this case AWS
  -  Provide the necessary Access Key and Secret Key to configure AWS Cloud Account
  -  Select Primary Region and Secondary Regions for discovery of resources and recovery
  -  Add the needed services from the cloud provider (Eg. EC2 , Classic LB and Network LB)
