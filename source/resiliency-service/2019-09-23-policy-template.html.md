---
title: Policy Template
layout: service_layout
sidebar_heading: Resiliency Service
sidebar_grouping: Concepts 
list: how-to-guides
---

Appranix protection policy defines when and how you want to protect your resources. You can decide which of the following two approaches best suits the needs of your environment

   - Default Policy Templates
   - Custom Policy Templates
   
### Default Policy Templates

 - Go to the Policy Template page
 - Enter a policy template name and, optionally, a description of your policy.
 - Select any of the following policy frequency type from the list

   - Organization Hourly
   - Organization Daily
   - Organization Weekly
   - Organization Monthly
   - Organization Yearly

-  In the Retention section, set a retention in counts for the time machine as the details given below.

   - Number of snapshots retained per storage volume in the primary region
   - Number of snapshots retained per storage volume in the recovery region

> **Note:**
> The recovery region replication and retentions are limited based on the cloud   provider.
> The replica retention count must be greater than or equal to retention count

-  Allow Override option provides permission to override the policy in cloud assembly. 

### Custom Policy Templates

Whenever the environment remains uncovered with **Default Policy Template**, you can create a new custom policy.
 
   


