---
title: Prerequisite for GCP
layout: service_layout
sidebar_heading: Resiliency Service
sidebar_grouping: Resiliency Service
pageHeading: Resiliency Service Documentation
list: prerequisite
---

###Granting Permissions to Discover Cloud Resources

To generate the required GCP credentials to use with the Appranix User Console, you need to create at least one GCP Identity and Access Management (IAM) user and assign proper permission policies to this user. You will have to obtain a GCP Project ID and a Service Account Configuration JSON, Object storage bucket name for your GCP account, which are the credentials to enter into the Appranix User Console for discovering all the account cloud resources.

**Appranix protect and recovery permission guidelines:**

   * Create a service account with the name 'appranix'. [Example : ‘appranix-protect-service’]
   * Create a key for the appranix service account

**Appranix bucket creation guidelines**

1. Provide the name for the bucket
2. Choose how to control access to objects -> Select 'Set permission uniformly at bucket-level'
3. Create the bucket
4. Select the permission tab -> Add 'appranix' service account has a member With  Role ' Storage Admin'

**NOTE:**  We required bucket to store/sync all the cloud asset details. The 'appranix' service account will only have Storage Admin permission to this bucket not for other buckets in your GCP storage.

**Cloud Asset API**

Enable the cloud asset API manages the history and inventory of cloud resources.

<figure class="concept_image">
  <img class="prerequisites" src="/images/prerequisites/gcp/cloud-asset-api.png" alt="Cloud Asset API" title="Cloud Asset API">
</figure>

**Adding CloudSQL permissions. Appranix requires the following prerequisites to protect the CloudSQL resources**

   * Project Id
   * Google JSON Key String

<p id="iam-policy"><strong>Apply IAM policy binding for the role shown below in the GCP supported regions</strong></p>

1. Compute Admin
2. Cloud Assert Viewer
3. Deployment Manager Editor
4. Serviceusage.services.use
5. Cloud SQL Admin

**As an example, to add an IAM policy binding to the service account ‘serviceAccount:project@example.com' use the following commands on the Cloud Shell**

<figure class="concept_image">
  <img class="prerequisites" src="/images/prerequisites/gcp/cloud-shell.png" alt="Cloud Shell" title="Cloud Shell">
</figure>

* **For the role of 'Compute Admin', run:**

        $ gcloud projects add-iam-policy-binding PROJECT_ID\--member 'serviceAccount:project@example.com' \--role 'roles/compute.admin'

* **For the role of 'Cloud Asset Viewer', run:**

        $ gcloud projects add-iam-policy-binding PROJECT_ID\--member 'serviceAccount:project@example.com' \--role 'roles/cloudasset.viewer'

* **For the role of 'Deployment Manager Editor', run:**

        $ gcloud projects add-iam-policy-binding PROJECT_ID\--member 'serviceAccount:project@example.com' \--role 'roles/deploymentmanager.editor'

* **For the custom role of 'serviceusage.services.use' permission, run:**

        $ gcloud iam roles create custom_role_service_usage --project PROJECT_ID\--title custom-role-service-usage --description\"Service usage" --permissions\Serviceusage.services.use
        $ gcloud projects add-iam-policy-binding PROJECT_ID\--member 'serviceAccount:project@example.com'\--role 'projects/PROJECT_ID/roles/custom_role_service_usage'

**This section summarizes permissions for the Cloud SQL support.**

* For the role of 'roles/cloudsql.admin', run:

        $ gcloud projects add-iam-policy-binding PROJECT_ID\--member 'serviceAccount:project@example.com'\--role 'roles/cloudsql.admin'