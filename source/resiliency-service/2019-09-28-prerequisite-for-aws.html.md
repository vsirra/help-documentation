---
title: Prerequisite for AWS
layout: service_layout
sidebar_heading: Resiliency Service
sidebar_grouping: Resiliency Service
pageHeading: Resiliency Service Documentation
list: prerequisite
---
### Protect the AWS Cloud Resources

To generate the required AWS credentials to use with the Appranix User Console, you need to create at least one AWS Identity and Access Management (IAM) user and assign proper permission policy to this user. You will have to obtain an AWS Access Key ID and a Secret Access Key for the AWS account, which are the credentials to enter into the Appranix User Console for discovering all the account cloud resources. 

Apply the IAM policy shown below in the AWS primary and recovery regions

  - IAM policy JSON details for discovering and managing Primary Region Resources
  - IAM policy JSON details for recovering resources in the Primary Region and Secondary Recovery Regions 
  - IAM policy JSON details for managing encrypted EBS volumes using AWS Key Management System (KMS) 
  - IAM policy JSON details for the Route-53 DNS

**Note:** Appranix doesn’t copy any customer Keys. All the keys are managed via AWS’s built-in Key Management System (KMS). You can configure a set of keys for your primary region and a separate set of keys for the recovery region as well.

### IAM policy JSON details for discovering and managing Primary Region Resources

    
    {
        "Version": "2012-10-17",
        "Statement": [{
                "Sid": "AppranixPrimaryRegionEc2AndElbReadAndSnapshotWriteAccess",
                "Effect": "Allow",
                "Action": [
                    "ec2:Describe*",
                    "ec2:CreateSnapshot",
                    "ec2:CreateTags",
                    "ec2:CopySnapshot",
                    "ec2:DeleteSnapshot",
                    "ec2:DeleteTags",
                    "elasticloadbalancing:Describe*"
                ],
                "Resource": "*",
                "Condition": {
                    "StringEquals": {
                        "aws:RequestedRegion": "Replace your primary region"
                    }
                }
            },
            {
                "Sid": "KmsCreateGrantAccess",
                "Effect": "Allow",
                "Action": "kms:CreateGrant",
                "Resource": "*",
                "Condition": {
                    "Bool": {
                        "kms:GrantIsForAWSResource": "true"
                    }
                }
            }
        ]
    }
    

### IAM policy JSON details for recovering resources in the Primary Region and Secondary Recovery Regions

    
    {
        "Version": "2012-10-17",
        "Statement": [{
                "Sid": "PrimaryRegionRecovery",
                "Effect": "Allow",
                "Action": [
                    "ec2:RunInstances",
                    "ec2:ModifyInstanceAttribute",
                    "ec2:TerminateInstances",
                    "ec2:RegisterImage",
                    "ec2:DeregisterImage",
                    "cloudformation:*"
                ],
                "Resource": "*",
                "Condition": {
                    "StringEquals": {
                        "aws:RequestedRegion": "Replace your primary or recovery region"
                    }
                }
            }
        ]
    }
    

### IAM policy JSON details for managing encrypted EBS volumes using AWS Key Management System (KMS)


    {
        "Id": "kms-describe-and-create-grant-policy",
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "CreateGrant",
                "Effect": "Allow",
                "Action": "kms:CreateGrant",
                "Resource": "*",
                "Condition": {
                    "Bool": {
                        "kms:GrantIsForAWSResource": "true"
                    }
                }
            },
            {
                "Sid": "AllowUseofTheKey",
                "Effect": "Allow",
                "Action": [
                    "kms:ListAliases",
                    "kms:DescribeKey*",
                    "kms:Encrypt",
                    "kms:Decrypt",
                    "kms:GenerateDataKey*"
                ],
                "Resource": "*"
            }
        ]
    }

### IAM policy JSON details for the Route-53 DNS


    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid" : "AllowHostedZoneListPermissions",
                "Effect": "Allow",
                "Action": [
                    "route53:GetHostedZone",
                    "route53:ListHostedZones",
                    "route53:GetHostedZoneCount",
                    "route53:ListHostedZonesByName"
                ],
                "Resource": "*"
            },
            {
                "Sid" : "AllowHostedZoneRecoredSetUpdatePermissions",
                "Effect": "Allow",
                "Action": [
                    "route53:ChangeResourceRecordSets",
                    "route53:ListResourceRecordSets",
                ],
                "Resource": ["arn:aws:route53:::hostedzone/Replace your hosted zone id"
                    ]
            }
        ]
    }


