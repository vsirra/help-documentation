---
title: Identity and Access Management System(IAM)
layout: service_layout
sidebar_heading: Resiliency Service
sidebar_grouping: Concepts
list: how-to-guides
---
### Managing Users 

The Manage Users option allows you to fully manage user accounts within your Appranix Account, including adding, removing, assigning, and deleting Users. Only the Account Owner and Account Admins can manage Users.

To create a user in your Appranix Account
 - Click the Create User button
 - Enter the details of the user (Eg. First Name, Last Name, Email, and UserName) 
 - Assign a role (Pre-created role) to the user

### Creating and managing custom roles
For security,  each feature of the Appranix Portal is designed to be available to users based on the project role assigned to the individual user. 

  You can also:
  - Select IAM in the navigation bar
  - Select User Roles in IAM
  - Enter the details for role creation (Role Name, Description)
  - Add the permissions to the roles. For more information, see User Roles Permissions
  
### User Roles Permissions 

* [Refer link for example](https://help.gooddata.com/doc/en/project-and-user-administration/managing-users-in-projects/user-roles/user-roles-permissions)
  
### Managing Account Profile
The Change Password option in My profile page opens the Change Password dialog box, which allows to instantly change the Account password.

### Configuring Single-Sign-On
SAML Integration enables the customer to get control of user management for their organization instead of using Appranix's default user management. Only the account admin can enable this feature.

To configure SAML Identity Provider in your Appranix Account

 - Select IAM in the navigation bar
 - Select Single Sign-On in IAM.
 - Check the Enable Single Sign-On checkbox.
 - Select Provider Type
 
**Step 1: Appranix Service Provider Details**

 - Appranix will provide the following details to register in Identity Provider
    - Entity ID
    - Assertion Consumer Service(ACS) URL
    - Encryption Certificate
            
**Step 2: Configure Identity Provider DetailS**

 - You will need to submit the following fields to Appranix for registering the Identity Provider
    - Entity ID
    - Single-Sign-On Service URL.
    - Signing Certificate
            
**Step-3: Configure Default Role Assignment**

 - Select the default role for the new user.



